import Vue from "vue";
import Router from "vue-router";
// Element
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(Router);
Vue.use(ElementUI);
// iview
import iView from "iview";
import "iview/dist/styles/iview.css"; // 使用 CSS
Vue.use(iView);

import 'bootstrap/dist/css/bootstrap.min.css'; // 引入bt的css
// import 'bootstrap/dist/js/bootstrap.min.js';

import home from "@/components/home/home";
import delivery from "@/components/about/delivery";
import history from "@/components/about/history";
import orderingGuide from "@/components/about/orderingGuide";

export default new Router({
  mode: "history", // 不配置就会加一个#，严格模式 依赖 HTML5 History API 和服务器配置
  base: __dirname, //__dirname(输出内容是/) 应用的基路径。例如，如果整个单页应用服务在 /app/ 下，然后 base 就应该设为 "/app/"。
//   scrollBehavior(to, from, savedPosition) {
    // 路由滚动
    // return {x:0, y:100}
    // 位置到第一个按钮的位置
    // return { selector: ".btn" };
    // 这里是指滚动到上一个页面的离开时的位置
    // if (savedPosition) {
    //   return savedPosition;
    // } else {
    //   return { x: 0, y: 0 };
    // }
//   },
  routes: [
    // 默认重定向一个路由
    {
      path: "/",
      name: "home",
      //   component: resolve => require(["@/components/home/home"], resolve)
      //  分解 => 需求
      components: {
        default: home,
        orderingGuide: orderingGuide,
        delivery: delivery,
        history: history
      }
    },
    {
      path: "/menu",
      name: "menu",
      component: resolve => require(["@/components/menu/menu"], resolve)
      //   beforeEnter: (to, form, next) => {
      //     // 路由独享守卫
      //   }
    },
    {
      path: "/about",
      name: "about",
      component: resolve => require(["@/components/about/about"], resolve),
      redirect: "/about/history", // 默认展示路由
      children: [
        {
          path: "contact",
          name: "contactLink",
          component: resolve =>
            require(["@/components/about/contact"], resolve),
          redirect: "/about/contact/personname",
          children: [
            {
              path: "phone",
              name: "phoneLink",
              component: resolve =>
                require([
                  "@/components/about/about_childer/contact_childer/phone"
                ], resolve)
            },
            {
              path: "personname",
              name: "personnameLink",
              component: resolve =>
                require([
                  "@/components/about/about_childer/contact_childer/personname"
                ], resolve)
            }
          ]
        },
        {
          path: "history",
          name: "historyLink",
          component: resolve => require(["@/components/about/history"], resolve)
        },
        {
          path: "delivery",
          name: "deliveryLink",
          component: resolve =>
            require(["@/components/about/delivery"], resolve)
          //   children: [
          //     {
          //       path: "delivery1",
          //       name: "delivery1Link",
          //       component: resolve =>
          //         require(["@/components/about/delivery1"], resolve)
          //     }
          //   ]
        },
        {
          path: "orderingGuide",
          name: "orderingGuideLink",
          component: resolve =>
            require(["@/components/about/orderingGuide"], resolve)
        }
      ]
    },
    {
      path: "/admin",
      name: "admin",
      component: resolve => require(["@/components/admin/admin"], resolve)
    },
    {
      path: "/login",
      name: "login",
      component: resolve => require(["@/components/login/login"], resolve)
    },
    {
      path: "/register",
      name: "register",
      component: resolve => require(["@/components/register/register"], resolve)
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
