import Vue from 'vue'
import Vuex from 'vuex'
import  * as actions from './actions'
// * as 引入文件全部方法    nuex中配置
import * as getters from './getters'
import * as mutations from './mutations'
import { state } from './state'
// 获取state文件里的对象，这里面不是方法，是属性
Vue.use(Vuex)
export const store = new Vuex.Store({
    // 分别应用
    state, // 设置属性
    getters, // 获取属性的状态
    mutations, // 改变属性的状态
    actions // 应用mutations
})