// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";

// vuex的使用
import { store } from "./store";

// axios
import axios from "axios";
axios.defaults.baseURL = "https://wd5122471827bdpsca.wilddogio.com/"; // 默认路径
// 配置Vue原型(在任何组件中都可以使用axios);
Vue.prototype.$axios = axios;

// Vue.config.productionTip = false

// 全局守卫
// router.beforeEach((to, from, next) => {
// to 进入哪个路由
// from从哪个路由离开
// next进入
// store.getters.isLogin 登录状态的存储
// if (store.getters.isLogin) {
//   next();
// } else {
// if (to.path == "/login" || to.path == "/register") {
//   next();
// } else {
//   next("login");
// }
// // }
// });

// 后置钩子 进入路由后
// router.afterEach((to, from) => {
// to 进入哪个路由
// from从哪个路由离开
//   alert('after Each')
// })

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   // router,
//   // store,

//   components: { App },
//   template: '<App/>'
// })

new Vue({
  el: "#app",
  router,
  store, // 不应用的话，组件中就无法使用
  render: h => h(App)
});
